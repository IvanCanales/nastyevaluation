module.exports = function(Usuario) {
    Usuario.getGruposActividades = function(usuarioId, cb) {
        Usuario.findById(usuarioId, {
            include: [{
                relation: 'grupos',
                    scope: {
                        include: {
                            relation: 'actividades'
                        }}
            }]
        },
        function(err, usuarioFound) {
            if (err)
                console.log(err);
            else {
                cb(null, usuarioFound)
            }
        });
    };

    Usuario.remoteMethod(
        'getGruposActividades', {
            http: {path: '/getGruposActividades', verb: 'get'},
            accepts: [{arg: 'UsuarioId', type: 'number'}],
            returns: {arg: 'actividades', type: 'object'}
        }
    );

    Usuario.getActividades = function(usuarioId, cb) {
        Usuario.findById(usuarioId, {
            include: [{
                relation: 'grupos',
                    scope: {
                        include: {
                            relation: 'actividades'
                        }}
            }]
        },
        function(err, usuarioFound) {
            if (err)
                console.log(err);
            else {
                var usuario = usuarioFound.toJSON();
                var grupos = usuario.grupos
                var actividades = []
                grupos.forEach(function(grupo) {
                    actividades.push(grupo.actividades);
                 });
                cb(null, actividades)
            }
        });
    };

    Usuario.remoteMethod(
        'getActividades', {
            http: {path: '/getActividades', verb: 'get'},
            accepts: [{arg: 'UsuarioId', type: 'number'}],
            returns: {arg: 'actividades', type: 'actividad'}
        }
    );

    Usuario.getUsuariowithRol = function(usuarioId, cb) {
        Usuario.findById(usuarioId, {
            fields: 
            {
                id: true, 
                nombre: true, 
                correo: true,
                contrasena: true,
                activo: true
            },
            include: [{
                relation: 'roles'
            }]
        },
        function(err, usuarioFound) {
            if (err)
                console.log(err);
            else {
                cb(null, usuarioFound)
            }
        });
    };

    Usuario.remoteMethod(
        'getUsuariowithRol', {
            http: {path: '/:UsuarioId/getUsuario-Rol', verb: 'get'},
            accepts: [{arg: 'UsuarioId', type: 'number'}],
            returns: {arg: 'usuario', type: 'object'}
        }
    );

    Usuario.getUsuarioswithRoles = function(cb) {
        Usuario.find({
            fields: 
            {
                id: true, 
                nombre: true, 
                correo: true,
                contrasena: true,
                activo: true
            },
            include: [{
                relation: 'roles'
            }]
        },
        function(err, usuariosFound) {
            if (err)
                console.log(err);
            else {
                cb(null, usuariosFound)
            }
        });
    };

    Usuario.remoteMethod(
        'getUsuarioswithRoles', {
            http: {path: '/:UsuarioId/getUsuarios-Roles', verb: 'get'},
            returns: {arg: 'usuario', type: 'object'}
        }
    );
};
