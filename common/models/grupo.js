module.exports = function(Grupo) {
	Grupo.getGruposUsuarios = function(cb) {
        Grupo.find({
            include: [{
                relation: 'usuarios'
            }]
        },
        function(err, gruposFound) {
            if (err)
                console.log(err);
            else {
                cb(null, gruposFound)
            }
        });
    };

    Grupo.remoteMethod(
        'getGruposUsuarios', {
            http: {path: '/:GrupoId/getGrupos-Usuarios', verb: 'get'},
            returns: {arg: 'grupo', type: 'object'}
        }
    );

    Grupo.getGruposActividades = function(cb) {
        Grupo.find({
            include: [{
                relation: 'actividades'
            }]
        },
        function(err, gruposFound) {
            if (err)
                console.log(err);
            else {
                cb(null, gruposFound)
            }
        });
    };

    Grupo.remoteMethod(
        'getGruposActividades', {
            http: {path: '/:GrupoId/getGrupos-Actividades', verb: 'get'},
            returns: {arg: 'grupo', type: 'object'}
        }
    );
};