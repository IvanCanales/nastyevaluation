module.exports = function(Evaluacion) {
	Evaluacion.getNotas = function(ActividadId, GrupoId, EvaluadoId, cb) {
        Evaluacion.find(
        	{where: 
        		{
        			actividadId: ActividadId, 
        			grupoId: GrupoId, 
        			evaluado: EvaluadoId
        		},
                include: ['usuario-evaluado', 'usuario-evaluador', 'actividad', 'grupo']
        	},
		cb);
    };

    Evaluacion.remoteMethod(
        'getNotas', {
            http: {path: '/getNotas', verb: 'get'},
            accepts: [
            {arg: 'ActividadId', type: 'number'},
            {arg: 'GrupoId', type: 'number'},
            {arg: 'EvaluadoId', type: 'number'}
            ],
            returns: {arg: 'notas', type: 'object'}
        }
    );

    Evaluacion.getEvaluados = function(ActividadId, GrupoId, EvaluadorId, cb) {
        Evaluacion.find(
            {where: 
                {
                    actividadId: ActividadId, 
                    grupoId: GrupoId, 
                    evaluador: EvaluadorId
                },
                include: ['usuario-evaluado', 'usuario-evaluador', 'actividad', 'grupo']
            },
        cb);
    };

    Evaluacion.remoteMethod(
        'getEvaluados', {
            http: {path: '/getEvaluados', verb: 'get'},
            accepts: [
            {arg: 'ActividadId', type: 'number'},
            {arg: 'GrupoId', type: 'number'},
            {arg: 'EvaluadorId', type: 'number'}
            ],
            returns: {arg: 'notas', type: 'object'}
        }
    );

    Evaluacion.getNotasGrupo_Actividad = function(ActividadId, GrupoId, cb) {
        Evaluacion.find(
            {where: 
                {
                    actividadId: ActividadId, 
                    grupoId: GrupoId
                },
                include: ['usuario-evaluado', 'usuario-evaluador', 'actividad', 'grupo']
            },
        cb);
    };

    Evaluacion.remoteMethod(
        'getNotasGrupo_Actividad', {
            http: {path: '/getNotasGrupo-Actividad', verb: 'get'},
            accepts: [
            {arg: 'ActividadId', type: 'number'},
            {arg: 'GrupoId', type: 'number'}
            ],
            returns: {arg: 'notas', type: 'object'}
        }
    );

    Evaluacion.getNotasGrupo_Evaluado = function(EvaluadoId, GrupoId, cb) {
        Evaluacion.find(
            {where: 
                { 
                    grupoId: GrupoId, 
                    evaluado: EvaluadoId
                },
                include: ['usuario-evaluado', 'usuario-evaluador', 'actividad', 'grupo']
            },
        cb);
    };

    Evaluacion.remoteMethod(
        'getNotasGrupo_Evaluado', {
            http: {path: '/getNotasGrupo-Evaluado', verb: 'get'},
            accepts: [
            {arg: 'EvaluadoId', type: 'number'},
            {arg: 'GrupoId', type: 'number'}
            ],
            returns: {arg: 'notas', type: 'object'}
        }
    );
};
