module.exports = function(Usuariohasgrupo) {
	Usuariohasgrupo.deleteUsuarioGrupo = function(UsuarioId, GrupoId, cb) {
        Usuariohasgrupo.destroyAll(
        	{usuarioId: UsuarioId, grupoId: GrupoId},
        	function(err, obj) {
	            if (err)
	                console.log(err);
	            else {
            		cb(null, obj);
	            }
        	});
		}

    Usuariohasgrupo.remoteMethod(
        'deleteUsuarioGrupo', {
        	accepts: 
        	[
	        	{arg: 'UsuarioId', type: 'number'},
	        	{arg: 'GrupoId', type: 'number'}
        	],
            returns: {arg: 'usuario-grupo', type: 'object'}
        }
    );
};
