module.exports = function(Grupohasactividad) {
	Grupohasactividad.deleteGrupoActividad = function(GrupoId, ActividadId, cb) {
        Grupohasactividad.destroyAll(
        	{grupoId: GrupoId, actividadId: ActividadId},
        	function(err, obj) {
	            if (err)
	                console.log(err);
	            else {
            		cb(null, obj);
	            }
        	});
		}

    Grupohasactividad.remoteMethod(
        'deleteGrupoActividad', {
        	accepts: 
        	[
	        	{arg: 'GrupoId', type: 'number'},
	        	{arg: 'ActividadId', type: 'number'}
        	],
            returns: {arg: 'grupo-actividad', type: 'object'}
        }
    );
};
