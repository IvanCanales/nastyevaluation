module.exports = function(Actividad) {
    Actividad.getUsuarios = function(actividadId, cb) {
        Actividad.findById(actividadId, {
            include: [{
                relation: 'grupos',
                    scope: {
                        include: {
                            relation: 'usuarios'
                        }}
            }]
        },
        function(err, actividadFound) {
            if (err)
                console.log(err);
            else {
                cb(null, actividadFound)
            }
        });
    };

    Actividad.remoteMethod(
        'getUsuarios', {
            http: {path: '/:ActividadId/getUsuarios', verb: 'get'},
            accepts: [{arg: 'ActividadId', type: 'number'}],
            returns: {arg: 'usuarios', type: 'object'}
        }
    );

    Actividad.getActividadesUsuarios = function(cb) {
        Actividad.find({
            include: [{
                relation: 'grupos',
                    scope: {
                        include: {
                            relation: 'usuarios'
                        }}
            }]
        },
        function(err, actividadFound) {
            if (err)
                console.log(err);
            else {
                cb(null, actividadFound)
            }
        });
    };

    Actividad.remoteMethod(
        'getActividadesUsuarios', {
            http: {path: '/:ActividadId/getActividadesUsuarios', verb: 'get'},
            returns: {arg: 'usuarios', type: 'object'}
        }
    );
};
