module.exports = function(Usuariohasrol) {
	Usuariohasrol.deleteUsuarioRol = function(UsuarioId, RolId, cb) {
        Usuariohasrol.destroyAll(
        	{usuarioId: UsuarioId, rolId: RolId},
        	function(err, usuario_rolFound) {
	            if (err)
	                console.log(err);
	            else {
            		cb(null, usuario_rolFound);
	            }
        	});
		}

    Usuariohasrol.remoteMethod(
        'deleteUsuarioRol', {
        	accepts: 
        	[
	        	{arg: 'UsuarioId', type: 'number'},
	        	{arg: 'RolId', type: 'number'}
        	],
            returns: {arg: 'usuario-rol', type: 'object'}
        }
    );
};
